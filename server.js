const mongo = require('mongodb').MongoClient;
const client = require('socket.io').listen(4000).sockets;

// Connect to mongodb
mongo.connect('mongodb://127.0.0.1/mongochat', (err, db) => {
    if (err) throw err;

    console.log('mongodb connected');

    // Connect to socket.io
    client.on('connection', (socket) => {
        let chat = db.collection('chats');

        // Create func to send status
        sendStatus = (s) => {
            socket.emit('status', s);
        }

        // Get chats from mongo collection
        chat.find().limit(100).sort({ _id: 1 }).toArray((err, result) => {
            if (err) throw err;

            //Emit the messages
            socket.emit('output', result);
        });

        //Handle input events
        socket.on('input', (data) => {
            let name = data.name;
            let messages = data.messages;

            //Check for name and message
            if (name === '' || messages === '') {
                // Send error status
                sendStatus('Please enter a name and message');
            } else {
                // Insert message 
                chat.insert({ name: name, message: messages }, () => {
                    client.emit('output', [data]);

                    //Send status object
                    sendStatus({
                        message: 'Message sent',
                        clear: true
                    });
                });
            }
        });

        // Handle clear 
        socket.on('clear', (data) => {
            // Remove all chats from collection
            chat.remove({}, () => {
                // Emit cleared
                socket.emit('cleared');
            })
        });
    });
});
